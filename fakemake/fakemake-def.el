;;;  -*- lexical-binding: t -*-

(defconst-with-prefix fakemake
  feature 'mmxx-macros
  authors "Dima Akater"
  first-publication-year-as-string "2022"
  org-files-in-order '( "mmxx-macros-appendix"

                        "mmxx-macros-00-preambles"
                        "mmxx-macros-01-basics"
                        "mmxx-macros-02-anaphora"
                        "mmxx-macros-03-dotree"
                        "mmxx-macros-04-introspection"
                        "mmxx-macros-05-defmacro"
                        "mmxx-macros-06-applications"

                        "mmxx-macros")
  site-lisp-config-prefix "50"
  license "GPL-3")

(defvar fakemake-ort-keep-going nil)

(advice-add 'fakemake-test :before
            (lambda ()
              (require 'org-src-elisp-extras)
              (require
               ;; this is only needed for tests in preambles
               'disass)
              (defvar ob-flags use-flags)
              ;; ditto
              (defun string-equal-modulo-whitespace (x y)
                (declare (pure t) (side-effect-free t))
                (cl-check-type x string)
                (cl-check-type y string)
                (cl-flet ((whitespace-char-p (char)
                            (cl-member char '(?\s ?\t ?\n) :test #'char-equal)))
                  (let ((i 0) (length-x (length x)) (j 0) (length-y (length y)))
                    (cl-flet ((skip-whitespace-in-x ()
                                (while (and (< i length-x) (whitespace-char-p (aref x i)))
                                  (cl-incf i)))
                              (skip-whitespace-in-y ()
                                (while (and (< j length-y) (whitespace-char-p (aref y j)))
                                  (cl-incf j))))
                      (cl-loop initially (skip-whitespace-in-x) (skip-whitespace-in-y)
                               do
                               (when (and (< i length-x) (whitespace-char-p (aref x i))
                                          (< j length-y) (whitespace-char-p (aref y j)))
                                 (skip-whitespace-in-x)
                                 (skip-whitespace-in-y))
                               (cond ((= length-x i) (skip-whitespace-in-y)
                                      (cl-return (= length-y j)))
                                     ((= length-y j) (skip-whitespace-in-x)
                                      (cl-return (= length-x i)))
                                     ((not (char-equal (aref x i) (aref y j)))
                                      (cl-return nil)))
                               (cl-psetf i (1+ i)
                                         j (1+ j)))))))
              (require 'cl-flet-improvement)
              (defun string-nonempty-p (string)
                (declare (pure t) (side-effect-free t))
                (unless (string-empty-p string) string))
              ))
