;;;; mmxx-macros.asd

(cl:in-package #:asdf)

(defsystem #:mmxx-macros
  :author "Dima Akater <nuclearspace@gmail.com>"
  :licence "GPL"
  :version "20250302"
  :serial t
  :description "A treatise on macros from 2020s by akater"
  :defsystem-depends-on (#:org-syntax)
  :depends-on (#:el.mmxx
               ;; this is a compile time dependency
               ;; more so, it is only a compile-time (or load-source-time) dependency
               ;; but the point is, if I specify it in :depends-on,
               ;; it doesn't seem to be loaded before el's compilation is started
               ;; so this dependency is specified in a method below, instead
               ;; #:org-block-language.emacs-lisp.host-backquote-only
               )
  :components (
               ;; (:org-cl-source-file "cl-package")
               (:org-cl-source-file "mmxx-macros-00-preambles")
               (:org-cl-source-file "mmxx-macros-01-basics")
               (:org-cl-source-file "mmxx-macros-02-anaphora")
               (:org-cl-source-file "mmxx-macros-03-dotree")
               (:org-cl-source-file "mmxx-macros-04-introspection")
               (:org-cl-source-file "mmxx-macros-05-defmacro")
               (:org-cl-source-file "mmxx-macros-06-applications")))

(let* ((mm (find-system "mmxx-macros"))
       (mm-introspection (find-component mm "mmxx-macros-04-introspection"))
       (mm-defmacro (find-component mm "mmxx-macros-05-defmacro"))
       (mm-applications (find-component mm "mmxx-macros-06-applications"))
       (mm-preambles (find-component mm "mmxx-macros-00-preambles"))
       (ob-elisp (find-system "org-block-language.emacs-lisp.host-backquote-only")))
  (defmethod component-depends-on ((o compile-op)
                                   ;; bad spec, yes
                                   (c (eql mm-preambles)))
    (declare (ignore o))
    (append (call-next-method)
            (list `(load-op ,ob-elisp))))
  (macrolet ((with-old-constants (&body body)
               `(handler-bind (
                               #+sbcl
                               (sb-ext:defconstant-uneql (lambda (condition)
				                           (when (find-restart 'sb-ext::abort condition)
				                             (invoke-restart 'sb-ext::abort))))
                               )
                  ,@body)))
    (defmethod around-compile-hook ((component (eql mm)))
      (declare (ignore component))
      (lambda (next) (with-old-constants (funcall next))))
    (defmethod around-compile-hook ((component (eql mm-introspection)))
      (declare (ignore component))
      (lambda (next) (with-old-constants (funcall next))))
    (defmethod around-compile-hook ((component (eql mm-applications)))
      (declare (ignore component))
      (lambda (next) (with-old-constants (funcall next))))

    (defmethod perform ((o load-op) (component (eql mm)))
      (declare (ignore o component))
      (with-old-constants (call-next-method)))

    (defmethod perform ((o load-op) (c (eql mm-introspection)))
      (declare (ignore o c))
      (with-old-constants (call-next-method)))

    (defmethod perform ((o compile-op) (c (eql mm-introspection)))
      (declare (ignore o c))
      (with-old-constants (call-next-method)))

    (defmethod perform ((o compile-op) (c (eql mm-defmacro)))
      (declare (ignore o c))
      (with-old-constants (call-next-method)))

    (defmethod perform ((o load-op) (c (eql mm-applications)))
      (declare (ignore o c))
      (with-old-constants (call-next-method)))

    (defmethod perform ((o compile-op) (c (eql mm-applications)))
      (declare (ignore o c))
      (with-old-constants (call-next-method)))))
